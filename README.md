# Mortgage Rate Scraper

Scrapes mortgage interest rate data from banking and broker websites.

## Requirements
- Python 3+
- pip

## To Run
1. Install dependencies `pip install -r requirements.txt`
2. Run `python main.py`

## Dev
- To update dependencies
    - `pip install pipreqs` if not installed 
    - `pipreqs -f` in project root