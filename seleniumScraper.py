from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service

# This is a test page for testing selenium searching. This is used for websites who
# change the page source before returning to beautiful soup in order to obscure their rates
# Since selenium operates directly on a browser, it can extract information from the
# page itself rather than the page source

options = Options()
options.headless = True
driver = webdriver.Firefox(options=options, service=Service('geckodriver.exe'))

driver.get("https://www.anz.co.nz/personal/home-loans-mortgages/")
title = driver.find_elements(by=By.CSS_SELECTOR, value='.FeaturedRates-Value')[-1]
import re
t = re.findall('\d*\.?\d+',title.text)[0]
print(t)

driver.quit()