from pageParser import PageParser
from scraper import Scraper as sp

# Run python main.py to run the program
# Use parser.print_all() to output a table of all results
# Use parser.parse_page('url key') to test individual pages

if __name__ == '__main__':

    urls = {
        "Westpac": ["https://www.westpac.co.nz/home-loans-mortgages/interest-rates/", sp.westpac],
        "Heartland": ["https://www.heartland.co.nz/home-loans/rates", sp.heartland],
        "SBS": ["https://www.sbsbank.co.nz/rates", sp.sbs],
        "HSBC": ["https://www.hsbc.co.nz/mortgages/products/floating-rate/", sp.hsbc],
        "ANZ": ["https://www.anz.co.nz/personal/home-loans-mortgages/", sp.anz],
        "ASB": ["https://www.asb.co.nz/home-loans-mortgages", sp.asb],
        "BNZ": ["https://www.bnz.co.nz/personal-banking/home-loans/compare-bnz-home-loan-rates?link=rates", sp.bnz],
        "Cooperative Bank": ["https://www.co-operativebank.co.nz/rates#borrow", sp.coop],
        "TSB": ["https://www.tsb.co.nz/loans/home-loans-mortgages/rates", sp.tsb],
        "Kiwibank": ["https://www.kiwibank.co.nz/personal-banking/home-loans/rates-and-fees/", sp.kiwibank],
        "Unity": ["https://unitymoney.co.nz/about-us/interest-rates/", sp.unity],
        "Heretaunga Building Society": ["https://www.heretaungabuildingsociety.co.nz/borrow/", sp.hbs],
        "Resimac": ["https://www.resimac.co.nz/en/rates-all#rates", sp.resimac]
    }

    parser = PageParser(urls)
    
    # print(parser.parse_page('Cooperative Bank'))
    # parser.parse_page('Sovereign')
    parser.print_all()