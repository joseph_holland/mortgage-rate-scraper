import os

class Defs:
    MSG = 'NaN'

    # ~~~ Selenium ~~~
    dirname = os.path.dirname(__name__)

    # Firefox
    FIREFOX_DRIVER = os.path.join(dirname,'drivers','geckodriver','geckodriver.exe')
