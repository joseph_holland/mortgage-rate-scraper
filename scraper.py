import json, re, requests
from telnetlib import EC
from django.test import tag

from bs4 import BeautifulSoup
from defs import Defs
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC

class Scraper:
    
    def get_decimal_from_string(string):
        try: 
            return re.findall('\d*\.?\d+',string)[0] 
        except:
            return -1

    def get_page_source(url): 
        return BeautifulSoup(requests.get(url).content, "html.parser")
    
    def scrape(url, function, use_selenium=False):
        driver = None
        try:
            if use_selenium:
                options = Options()
                options.headless = True
                driver = webdriver.Firefox(options=options, service=Service(Defs.FIREFOX_DRIVER))
                driver.get(url)
                val = function(driver)
            else:
                val = function(Scraper.get_page_source(url))

            return Scraper.get_decimal_from_string(val)
        except:
            return Defs.MSG
        finally:
            if driver:
                driver.quit()

    # ~~~ BEAUTIFULSOUP BASED ~~~

    def westpac(url):

        def scrape(page):
            results = page.find(id="base-rates").find(class_="js-charge-table-block")

            js = json.loads(results['data-props'])

            i = -1
            for x in range(len(js['rows'])):
                if "Housing Base Rate" in js['rows'][x][0]['value']:
                    i = x

            return js['rows'][i][1]['value']

        return Scraper.scrape(url, scrape)

    def heartland(url):
        def scrape(page):
            return page.find(text='Revolving credit (floating)').findNext('h3').contents[0]
        return Scraper.scrape(url, scrape)

    def sbs(url):
        def scrape(page): 
            return page.find(text='Residential Floating Rate / First Home Loan Floating Rate').findNext('td').contents[0]
        return Scraper.scrape(url, scrape)
        
    def hsbc(url):
        def scrape(page): 
            return page.find(text='Floating rate').findNext('td').contents[0]
        return Scraper.scrape(url, scrape)

    def tsb(url):
        def scrape(page: BeautifulSoup):
            return page.find(text='Housing Variable').parent.find_next('td').text
        return Scraper.scrape(url, scrape)
    
    def unity(url):
        def scrape(page: BeautifulSoup):
            return page.find_all('td', text='Floating')[-1].find_next('td').text
        return Scraper.scrape(url, scrape)

    def hbs(url):
        def scrape(page:BeautifulSoup):
            return page.find('span', text='Floating').find_next('span').text
        return Scraper.scrape(url, scrape)

    def resimac(url):
        def scrape(page:BeautifulSoup):
            return page.find('tr', class_='loan').find_all('td')[1].text
        return Scraper.scrape(url, scrape)

    def sovereign(url):
        def scrape(page:BeautifulSoup):
            print('test')
            # e = page
            # print(e)
            return -1
        return Scraper.scrape(url, scrape)

    # ~~~ SELENIUM BASED ~~~

    def anz(url):
        def scrape(driver):
            elem = driver.find_elements(by=By.CSS_SELECTOR, value='.FeaturedRates-Value')[-1]
            return elem.text

        return Scraper.scrape(url, scrape, use_selenium=True)
        
    def asb(url):
        def scrape(driver):
            tag = driver.find_element(by=By.XPATH, value="//*[contains(text(), 'Housing variable rate')]/preceding-sibling::span[1]")
            number_base = tag.find_element(by=By.CSS_SELECTOR, value=".number")
            number = number_base.get_attribute('innerHTML').split('<')[0].strip()
            decimal = number_base.find_element(by=By.CSS_SELECTOR, value=".decimal").get_attribute('innerHTML').strip()

            return (number+decimal)
        
        return Scraper.scrape(url, scrape, use_selenium=True)
    
    def bnz(url):
        def scrape(driver):
            table = driver.find_element(by=By.ID, value='compare-rates').find_element(by=By.TAG_NAME, value='table')
            tbody = table.find_element(by=By.TAG_NAME, value='tbody')
            rate = tbody.find_element(by=By.CSS_SELECTOR, value='tr:nth-child(1) > td:nth-child(3)')
            return rate.get_attribute('innerHTML')
        
        return Scraper.scrape(url, scrape, use_selenium=True)

    def coop(url):
        def scrape(driver):
            css = 'div[data-slot-name="homeLoans"]'
            wait(driver, 20).until(EC.visibility_of_element_located(
                (By.CSS_SELECTOR, css)
            ))
            return driver.find_element(By.CSS_SELECTOR, value=css).text.split('\n')[4]

        return Scraper.scrape(url, scrape, use_selenium=True)

    def kiwibank(url):
        def scrape(driver):
            v = driver.find_elements(By.TAG_NAME, value="a")
            
            e = driver.find_element(By.CSS_SELECTOR, value='div.scrolling-table-wrapper:nth-child(2) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(7) > td:nth-child(2) > p:nth-child(1) > span:nth-child(1) > span:nth-child(1) > span:nth-child(1)')
            # e = driver.find_element(By.CSS_SELECTOR, value='site-container')
            driver.save_screenshot('ss.png')
            p = None
            for a in v:
                if a.text == 'Variable':
                    p = a
            elem = p.find_element(By.XPATH, value='..').find_element(By.XPATH, value='../following-sibling::td')
            rate = elem.find_element(By.CSS_SELECTOR, value='.rate__value').text
            return rate
        return Scraper.scrape(url, scrape, use_selenium=True)