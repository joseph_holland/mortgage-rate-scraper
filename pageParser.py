import operator, textwrap

# For interpreting the data and outputting it on a nice view
# Will eventually swap out for some actual visuals like matplotlib or wxpython

class PageParser:
    def __init__(self, urls):
        self.urls = urls

    def parse_page(self, name):
        return [
            name, 
            self.urls[name][1]( url=self.urls[name][0])
        ]
    
    def print_all(self):
        name_len = len(max(self.urls.keys(), key=len))
        sep = "="*(name_len + 12)

        def print_line(line):
            if type(line) is list:
                print("| {0} | {1} |".format(*line))
            else:
                print("| {0} |".format(line.ljust(len(sep)-4, " ")))

        def print_heading(heading):
            lines = textwrap.wrap(heading, width=len(sep) - 4, break_long_words=True)

            print(sep)
            for s in lines: print("| {0} |".format(s.ljust(len(sep) - 4, " ")))
            print(sep)
        
        def get_results():
            floating = []
            unavailable = []

            for k in self.urls.keys():
                name = k.ljust(name_len+1, " ")
                num = self.parse_page(k)[1]
                if num == -1:
                    unavailable.append(name)
                else:
                    num_str = str(num).ljust(4, " ")
                    floating.append([name, num_str])
            return sorted(floating, key=operator.itemgetter(1)), unavailable

        floating,unavailable = get_results()
        
        print_heading('Floating Rates')
        for f in floating: print_line(f)
        print(sep)
        if unavailable:
            print_heading('These rates are unavailable due to hidden or anti-scraper code')
            for u in unavailable: print_line(u)
        print(sep)
